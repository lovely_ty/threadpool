#pragma once
#include "stddef.hpp"
#include "task_t.hpp"

class task_queue_t
{
public:
	task_queue_t(void);
	~task_queue_t(void);
	void push_back(task_ptr_t &task);
	bool pull_front(task_ptr_t &task_);
private:
	task_queue_t(const task_queue_t &);
	const task_queue_t &operator=(const task_queue_t &);

	typedef std::queue<task_ptr_t> task_list_t;
	typedef std::lock_guard<std::mutex> lock_guard_t;
	
	task_list_t task_queue;
	std::mutex queue_mutex;
};
