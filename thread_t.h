#pragma once
#include "stddef.hpp"
#include "task_t.hpp"

class thread_t
{
public:
	typedef std::function<bool(task_ptr_t &)> pull_task_fun_t;
	typedef std::function<void(uint32_t)> exit_fun_t;

	typedef std::lock_guard<std::mutex> lock_guard_t;
	typedef std::unique_lock<std::mutex> unique_lock_t;

	enum {suspend, runing, stoping};
	
	thread_t(uint32_t tid_, pull_task_fun_t &fun_, exit_fun_t exit_fun_);
	~thread_t(void);
	void set_idle_timeout(uint32_t idle_timeout_);
	int get_status();
	void run(task_ptr_t &task);
	void stop();
	void start();
	uint32_t get_tid();
private:
	void detach();
	void loop();
	void wake_up();
	void to_sleep();
	void to_work();
	void wait_timeout();
	std::shared_ptr<std::thread> thread;
	std::mutex mutex;
	std::mutex cond_var_mutex;
	std::condition_variable cond_var;
	task_ptr_t task;
	std::atomic_int status;
	uint32_t idle_timeout;
	pull_task_fun_t pull_task;
	exit_fun_t exit_fun;
	uint32_t tid;
	std::atomic_bool b_run;
};
typedef std::shared_ptr<thread_t> thread_ptr_t;